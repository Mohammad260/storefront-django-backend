from django.shortcuts import render
from .tasks import notify_customers
from store.models import Product, Order

def say_hello(request):
   notify_customers.delay('hello')
   
   product = Order.objects.select_related('customer').prefetch_related('orderitem_set__product').order_by('-placed_at')[:5]
   return render (request, "hello.html", {
        "name": "World", 'products': []
    })
